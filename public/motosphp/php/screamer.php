 <!DOCTYPE html>
<html lang="es">
    <head>

      <meta charset="UFT-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">

      <!--Bootstrap v5.0.2-->
      <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

      <!--Style-->
      <link rel="stylesheet" href="/motos/assets/styles/iFrameStyle.css">

      <!--Scripts-->
      <script src="/motos/assets/scripts/ModelViewerPopUP.js"></script>
      <script src="https://unpkg.com/focus-visible@5.0.2/dist/focus-visible.js" defer></script>
      <script src="/motos/assets/scripts/Clicks513468.js"></script>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

      <!--Model Viewer-->
      <script type="module" src="https://unpkg.com/@google/model-viewer/dist/model-viewer.min.js"></script>
      <script nomodule src="https://unpkg.com/@google/model-viewer/dist/model-viewer-legacy.js"></script>

      <!--TimeMe JS-->
      <script src="/motos/assets/scripts/timeme.min.js"></script>
      <script src="/motos/assets/scripts/TimeMe513468.js"></script>

      <title>Motocicletas AR Coppel</title>
    </head>
    <body>
        <header class="Myheader">

          <div class="row top-buffer">
            <div class="col"></div>
            <h1>Motocicletas Realidad Aumentada</h1>
          </div>

          <div class="row">
            <div class="col"></div>
            <h4>By</h4>
          </div>

            <div class="container">
                <div class="row align-items-center">
                  <div class="col d-flex justify-content-start">
                    <a class="navbar-brand" onClick="divFunction2('/motos/index.php','SCREAMER')">
                      <img class="img-fluid" src="/motos/Resources/UI/icon-return.png" width="30" height="30">
                    </a>
                  </div>

                  <div class="col d-flex justify-content-end">
                    <a class="navbar-brand" href="./index.php">
                        <img class="img-fluid" src="/motos/Resources/Logos/Logo-coppel.png" width="110" height="70">
                    </a>
                  </div>

                  <div class="col d-flex justify-content-center">
                    <button class="button button1" onClick="window.location.href = 'https://www.coppel.com/motocicleta-vento-screamer-250-cc-2022-pm-5134683';">
                      Ir a producto
                    </button>
                  </div>

                  <div class="col d-flex justify-content-start">
                    <a class="navbar-brand" href="./index.php">
                        <img class="img-fluid" src="/motos/Resources/Logos/Logo-CENICNOBG.png" width="150" height="50">
                    </a>
                  </div>

                  <div class="col d-flex justify-content-end">
                    <a class="navbar-brand" href="https://docs.google.com/forms/d/e/1FAIpQLSdfBsUWdXZ99eMKDnccY9vl8gCjZvaDi1PegSEbLtCzz8y0XA/viewform">
                      <img class="img-fluid" src="/motos/Resources/UI/icon-poll 00.png" width="30" height="30">
                    </a>
                  </div>
                </div>
            </div>
        </header>
        <div class="ratio ratio-16x9">
          <model-viewer ar ar-modes="webxr scene-viewer quick-look"
          src="/motos/Resources/Motorcycles/Screamer.glb"
          ios-src="/motos/Resources/Motorcycles/Screamer.usdz"
          xr-environment
          shadow-intensity="1"
          id="transform"
          ar ar-scale="fixed"
          camera-controls disable-zoom
          ar ar-placement="floor" 
          auto-rotate ar>
          
          <button slot="ar-button" style="font-size: 1.5vw;" id="ar-button"> Ver en realidad aumentada </button>
          <button class="button button4" style="font-size:1.6vw;" onClick="Open()">Características</button>
          <div class="ventana3" id="vent">
            <div id="cerrar"> <a href="javascript:Close()"><img src="/motos/Resources/UI/cruzb.png" width="5.5%" style="margin-left: 90%; margin-top: 5%"></a> </div>
            <h1 style="font-size:1.8vw;">Vento Screamer 2022</h1>
            <p style="font-size:1.3vw;">Cilindrada: 250cc</p>
            <p style="font-size:1.3vw;">Velocidad máxima: 130 km/h</p>
            <p style="font-size:1.3vw;">Transmisión: Manual</p>
            <p style="font-size:1.3vw;">Autonomía: 540 km</p>
            <p style="font-size:1.3vw;">Rendimiento combustible: 30 km/l</p>
            <p style="font-size:1.3vw;">Tipo de Faro: Redondo multi-lupa</p>
            <p style="font-size:1.3vw;">Tipo Luz de Faro: LED</p>
          </div>
          
          </model-viewer>
        </div>

          <div>
            <footer class=" text-center MyFooter">
              <!-- Copyright -->
              <h3 class="text-center p-3">
                Motocicletas Realidad Aumentada © 2022 Copyright Grupo Coppel:
                <a class="text-dark" href="https://www.coppel.com/aviso-de-privacidad">Todos los derechos reservados</a>
              </h3> 
            </footer>
          </div>          
    </body>
    <script type="text/javascript" src="/motos/js/script.js"></script>
<?php
# Incluir el contador de visitas
include_once "contador_visita.php";

$visitas = file_get_contents("contador.txt");
printf("<h1>Visitantes: %d ", $visitas);
?>
</html>
